import Vue from 'vue'
import App from './App.vue'
import router from './router'
import animate from 'animate.css'
import VueLazyload from 'vue-lazyload'

import '@/assets/css/index.scss'
import '@/assets/font/font.css'

Vue.config.productionTip = false
Vue.use(animate)
Vue.use(VueLazyload, {
  preLoad: 3,
  attempt: 7,
  // error: 'dist/error.png',
  // loading: 'dist/loading.gif',
  listenEvents: ['scroll']
})
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
