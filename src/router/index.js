import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      order: 1
    }
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/About.vue'),
    meta: {
      order: 5
    }
  },
  {
    path: '/lucky',
    name: 'Lucky',
    component: () =>
      import(/* webpackChunkName: "lucky" */ '../views/Lucky.vue'),
    meta: {
      order: 2
    }
  },
  {
    path: '/app',
    name: 'App',
    component: () => import(/* webpackChunkName: "koi" */ '../views/Koi.vue'),
    meta: {
      order: 3
    }
  },
  {
    path: '/experience',
    name: 'Experience',
    component: () => import(/* webpackChunkName: "AboutMe" */ '../views/Experience.vue'),
    meta: {
      order: 4
    }
  },
  // {
  //   path: '/three',
  //   name: 'three',
  //   component: () => import(/* webpackChunkName: "koi" */ '../components/spline.vue')
  // },
  {
    path: '*',
    redirect: '/'
  }
]
console.log(process.env.VUE_APP_BASE_URL)
const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
})

const VueRouterPush = VueRouter.prototype.push

VueRouter.prototype.push = function push (to) {
  return VueRouterPush.call(this, to).catch((err) => err)
}
export default router
