const title = 'BinBinPortfolio'
const { resolve } = require('path')
const isPro = process.env.NODE_ENV === 'production'
module.exports = {
  // publicPath: isPro ? '/zhoubinbin.top' : '/',
  lintOnSave: true,
  productionSourceMap: false,
  assetsDir: 'dist',
  css: {
    extract: true,
    sourceMap: false
  },
  devServer: {
    open: true,
    hot: true
  },
  pluginOptions: {
    // 设置全局变量
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [resolve(__dirname, './src/assets/css/variable.scss')]
    }
  },
  chainWebpack: (config) => {
    // 配置路径别名
    config.resolve.alias.set('@', resolve('./src'))

    // 设置title
    config.plugin('html').tap((args) => {
      args[0].title = title
      return args
    })

    // 10kb以内的图片会被打包成内联元素
    config.module
      .rule('images')
      .use('url-loader')
      .loader('url-loader')
      .tap((options) => Object.assign(options, { limit: 10240 }))

    if (isPro) {
      // 压缩代码
      config.optimization.minimize(true)
      // 分割代码
      config.optimization.splitChunks({
        chunks: 'all'
      })
    }
  }
}
